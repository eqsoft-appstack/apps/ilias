#!/usr/bin/env bash

SERVER_JAR="/var/www/html/Services/WebServices/RPC/lib/ilServer.jar"

if [ -f "${SERVER_JAR}" ] ; then
	echo "${SERVER_JAR} already exists..."
else
	cd /var/www/html/Services/WebServices/RPC/lib ; mvn clean install
	mv /var/www/html/Services/WebServices/RPC/lib/target/ilServer.jar /var/www/html/Services/WebServices/RPC/lib/
	rm -rf /var/www/html/Services/WebServices/RPC/lib/target
fi
