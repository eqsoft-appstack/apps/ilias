#!/usr/bin/env php

# executed in php container with mounted volumes pwd, ilias webroot

<?php

$pluginConfigsFile = "/tmp/config/plugins.json";

$pluginConfigsContent = file_get_contents($pluginConfigsFile);
$pluginConfigs = json_decode($pluginConfigsContent, true);

$pluginsPath = "/var/www/html/Customizing/global/plugins";
if (!file_exists($pluginsPath)) {
   shell_exec("mkdir -p {$pluginsPath}");
}

foreach ($pluginConfigs as $pluginConfig) {
    $fullPluginPath = "{$pluginsPath}/{$pluginConfig['pluginPath']}";
    if (file_exists($fullPluginPath)) {
        echo "\nPlugin {$pluginConfig['name']} already loaded\n";
        continue;
    } else {
        echo "\nLoad plugin {$pluginConfig['name']}\n";
    }
    $gitUrl = preg_replace_callback('/\$\{(.*?)\}/',function($matches){return getenv($matches[1]);},$pluginConfig['gitUrl']);
    $gitOpts = (empty($pluginConfig['gitOpts'])) ? "" : $pluginConfig['gitOpts'];
    $cmd = "git clone {$gitOpts} {$gitUrl} ./{$pluginConfig['pluginPath']}\n";
    chdir($pluginsPath);
    $wd = getcwd();
    echo "in {$wd}:\n{$cmd}\n";
    shell_exec($cmd);
    if (array_key_exists('patchFileDirPath', $pluginConfig)) {
        $it = new RecursiveDirectoryIterator("{$fullPluginPath}/{$pluginConfig['patchFileDirPath']}");
        $extension = array('patch');
        foreach (new RecursiveIteratorIterator($it) as $file) {
            $fileArr = explode('.', $file);
            if (in_array(strtolower(array_pop($fileArr)), $extension)) {
                echo "Apply patch file {$file}\n";
                chdir("/var/www/html");
                $cmd = "git apply {$file}";
                $wd = getcwd();
                echo "\nin {$wd}:\n{$cmd}\n";
                shell_exec($cmd);
            }
        }
    }    
    
    $cmd = "composer update";
    chdir($fullPluginPath);
    $wd = getcwd();
    if (file_exists("{$wd}/composer.json")) {
        echo "\nin {$wd}:\n{$cmd}\n";
        shell_exec($cmd);
        $cmd = "composer du";
        echo "\nin {$wd}:\n{$cmd}\n";
        shell_exec($cmd);
    }
}
