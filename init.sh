#!/usr/bin/env bash

# init before stack is starting

. ./env

echo "create xdebug.ini"
    envsubst '\
    ${STACK}\
' < ./files/config/tpl.xdebug.ini > ./files/config/xdebug.ini

echo "init ilias config.json"

$CONTAINER_ENV run --rm --network=${NET} -w /tmp/config -v ${APP_ID}-webroot:/tmp/html -v ${APP_ID}-data:/tmp/data -v $(pwd)/files/config:/tmp/config -u root --env-file=.env ${CONFIG_IMAGE} ./config.sh
$CONTAINER_ENV run --rm --network=${NET} -w /tmp/config -v ${APP_ID}-webroot:/var/www/html -v $(pwd)/files/config:/tmp/config --env-file=.env ${ENV_PHP_IMAGE} ./load-plugins.php
$CONTAINER_ENV run --rm --network=${NET} -w /tmp/config -v ${APP_ID}-webroot:/var/www/html -v $(pwd)/files/config:/tmp/config --env-file=.env ${ENV_PHP_IMAGE} ./build-rpc.sh
