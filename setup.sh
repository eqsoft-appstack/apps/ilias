#!/usr/bin/env bash

# setup after stack is started

. ./env

# check if ilias.ini.php exists from within php container

ILIAS_INI_EXISTS=$($CONTAINER_ENV exec -t -w /var/www/html ${APP_ID}-php [ -f ilias.ini.php ] && echo 1 )
NODE_MODULES_EXISTS=$($CONTAINER_ENV exec -t -w /var/www/html ${APP_ID}-php [ -d node_modules ] && echo 1 )

if [ "$ILIAS_INI_EXISTS" == "1" ] ; then
    echo "ilias.ini.php already exists, skipping initial ilias setup..."
else
    echo "initial ilias setup..."
    $CONTAINER_ENV exec -u root ${APP_ID}-java chown -R www-data:root /lucene
    $CONTAINER_ENV exec -t -w /var/www/html ${APP_ID}-php composer install -n
    $CONTAINER_ENV exec -t -w /var/www/html ${APP_ID}-php php setup/setup.php install -y /var/www/data/config.json
fi
if [ "$NODE_MODULES_EXISTS" == "1" ] ; then
    echo "node_modules already exists, skipping initial npm setup..."
else
    $CONTAINER_ENV exec -t -w /var/www/html ${APP_ID}-php /bin/bash -c "npm clean-install"
fi

